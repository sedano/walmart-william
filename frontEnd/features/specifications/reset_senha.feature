# language: pt

Funcionalidade: Reset Senha
@resetar_senha
Cenario: Resetar a senha

	Dado que eu abra a caixa de e-mail da Temp Mail
	Quando clicar no botao copiar
	E Abrir página de reset de senha
	E Preencher o campo e-mail com o seu e-mail temporário
	E Clicar no botão Retrieve password
	E Abrir a caixa de email novamente do Temp Mail
	E Clicar no botão Refresh
	Entao validar a nova mensagem na sua caixa de entrada
