 require 'pry'
 # encoding: utf-8 (colocar o cerquilha antes)
 # !/usr/bin/env ruby (colocar o cerquilha antes)
 Before do
 	include Capybara::DSL	
 	@email = Faker::Internet.email('testreset')
 end

 Dado(/^que eu abra a caixa de e-mail da Temp Mail$/) do
 	visit ''
 end

 Quando(/^clicar no botao copiar$/) do
 	find(:id,'click-to-copy').click
 end
 
 E(/^Abrir página de reset de senha$/) do
 	visit 'https://the-internet.herokuapp.com/forgot_password'
 end

E(/^Preencher o campo e-mail com o seu e-mail temporário$/) do
 	find(:id, 'email').set(@email) 
 	puts 'O email utilizado é: ' + @email
 end

 E(/^Clicar no botão Retrieve password$/) do
 	click_on 'Retrieve password'
 end

  E(/^Abrir a caixa de email novamente do Temp Mail$/) do
	visit ''
 end

 E(/^Clicar no botão Refresh$/) do
 	find(:id, 'click-to-refresh').click
 end

 Então(/^validar a nova mensagem na sua caixa de entrada$/) do
 	expect(page).to have_content('Temp Mail')
 	puts 'Reenvio de senha com sucesso'
 end
